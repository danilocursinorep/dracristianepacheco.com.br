<?php get_header(); ?>
	<?php while(have_posts()): the_post(); ?>
	
	<section class="ebookposts">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-lg-8">
					<div class="container">
						<h2 class="h2">E-books</h2>
						<?php
							$paged = (get_query_var('paged'))? get_query_var('paged'): 1;
							$query = new WP_Query(array( 
								'post_type' => 'ebook',
								'posts_per_page' => 3,
								'order' => 'DESC',
								'paged' => $paged 
							));
						?>
						<?php if($query->have_posts()): ?>
						<div class="itens">
						<?php while($query->have_posts()): $query->the_post(); ?>
							<div class="item">
								<img class="folha" src="<?php echo get_template_directory_uri(); ?>/assets/img/folhablog.png">
								<div class="row">
									<div class="align-self-center col-12 col-sm-7 img">
										<a href="<?php the_permalink(); ?>">
											<?php the_post_thumbnail(); ?>
										</a>
									</div>	
									<div class="align-self-center col-12 col-sm-5 cnt">
										<a href="<?php the_permalink(); ?>">
											<h2 class="title"><?php the_title(); ?></h2>
										</a>
									</div>
									<div class="align-self-center col-12">
										<a href="<?php the_permalink(); ?>">
											<?php the_field('descricao'); ?>
											<?php the_field('itens'); ?>
											<button class="button">Baixe gratuitamente</button>
										</a>
									</div>
								</div>
							</div>
						<?php endwhile; ?>
						</div>
						<div class="paginacao">
        						<?php pagination($query) ?>
						</div>
						<?php else: ?>
							<h3 class="h3">Ainda não há nenhum blogpost.<br/> Volte novamente mais tarde para novidades.</h3>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php endwhile; ?>
<?php get_footer();