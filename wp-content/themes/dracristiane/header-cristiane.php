<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Clínica_ELO
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/site.webmanifest">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

	<?php wp_head(); ?>
</head>

<?php global $post; ?>
<?php $slug = $post->post_name; ?>
<body <?php body_class($slug); ?>>
<?php wp_body_open(); ?>

<div id="page" class="site">

	<header id="masthead" class="site-header">
		<div class="floatMenu page">
			<div class="container">
				<div class="row">
					<div id="navegacao" class="col-6">
						<div class="content">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ball.png" class="ball" />
							<a href="<?php echo home_url(); ?>" target="_self">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" class="logo" />
							</a>
							<nav id="site-navigation">
			                        	<div id="menu-icone">
			                            <span></span>
			                            <span></span>
			                            <span></span>
			                            <span></span>
			                        </div>
								<div class="menu-container">
									<?php wp_nav_menu(
										array(
											'theme_location' => 'menu-1',
											'menu_id' => 'primary-menu'
										)
									); ?>
								</div>
							</nav>
						</div>
					</div>
					<div id="search" class="col-6 align-self-center">
						<?php get_search_form(); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="contentTop">
			<div class="container">
				<div class="row justify-content-start">
					<blockquote class="col-5 col-lg-4 offset-lg-1">
						<img class="logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/cristiane.png" />
						<span>CRM-AM 3.506 | RQE 1.488</span>
					</blockquote>
				</div>
			</div>
		</div>
		<div id="topo"<?php echo is_singular('post')?' class="site"':''; ?>>
			<div class="conteudo dra">
				<img class="mascara" src="<?php echo get_template_directory_uri(); ?>/assets/img/dracristiane.png" />
			</div>
		</div>
	</header><!-- #masthead -->
	<main id="main" class="site-main">