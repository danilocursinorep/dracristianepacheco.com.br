<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Clínica_ELO
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/site.webmanifest">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

	<?php wp_head(); ?>
</head>

<?php global $post; ?>
<?php $slug = $post->post_name; ?>
<body <?php body_class($slug); ?>>
<?php wp_body_open(); ?>

<div id="page" class="site">

	<header id="masthead" class="site-header">
		<div class="floatMenu page">
			<div class="container">
				<div class="row">
					<div id="navegacao" class="col-6">
						<div class="content">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ball.png" class="ball" />
							<a href="<?php echo home_url(); ?>" target="_self">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" class="logo" />
							</a>
							<nav id="site-navigation">
			                        	<div id="menu-icone">
			                            <span></span>
			                            <span></span>
			                            <span></span>
			                            <span></span>
			                        </div>
								<div class="menu-container">
									<?php wp_nav_menu(
										array(
											'theme_location' => 'menu-1',
											'menu_id' => 'primary-menu'
										)
									); ?>
								</div>
							</nav>
						</div>
					</div>
					<div id="search" class="col-5 offset-1 col-md-6 offset-md-0 align-self-center">
						<?php get_search_form(); ?>
					</div>
				</div>
			</div>
		</div>
		<?php if (is_404()): ?>
		<div class="contentTop">
			<div class="container">
				<div class="row justify-content-end">
					<blockquote class="col-12 col-lg-4">Ops,<br> página não<br> encontrada :-(</h2></blockquote>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<div id="topo"<?php echo is_singular('post')?' class="site"':''; ?>>
			<div class="conteudo">
				<div class="destacada"  style="background-image: url(<?php
					if (is_page('contato')) {
						echo get_template_directory_uri() . "/assets/img/topopaginas/contato.jpg";
					} elseif (is_page('blog')) {
						echo get_template_directory_uri() . "/assets/img/topopaginas/blogs.jpg";
					} elseif (is_page('ebooks')) {
						echo get_template_directory_uri() . "/assets/img/topopaginas/blogs.jpg";
					} elseif (is_page('agendamento')) {
						echo get_template_directory_uri() . "/assets/img/topopaginas/ag.jpg";
					} elseif (is_404()) {
						echo get_template_directory_uri() . "/assets/img/topopaginas/404.jpg";
					} elseif (is_search()) {
						echo get_template_directory_uri() . "/assets/img/topopaginas/search.jpg";
					} else {
						the_post_thumbnail_url();
					}
				?>);"></div>
				<img class="mascara" src="<?php echo get_template_directory_uri(); ?>/assets/img/<?php echo is_singular('post')?'folhatopofundo':'topoPage'; ?>.png" />
			</div>
		</div>
	</header><!-- #masthead -->
	<main id="main" class="site-main">