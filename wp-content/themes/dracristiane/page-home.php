<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Dra._Cristiane_Pacheco
 */

get_header('home'); ?>

<section class="agendamento">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-8">
				<h2 class="h2">Pré-agendamento</h2>
			</div>
			<div class="col-12 col-lg-7">
				<form class="forms box-shadow">
					<span class="txt txt1">Humanização, personalização e disponibilidade. Um atendimento ginecológico e obstétrico com mais empatia.</span>
					<span class="txt txt2">Preencha seus dados abaixo e nossa equipe entrará em contato<br> para realizar o seu agendamento.</span>
					<div class="row">
						<div class="col-12 col-lg-6">
							<input type="text" placeholder="Nome completo" />
							<input type="text" placeholder="Celular" />
						</div>
						<div class="col-12 col-lg-6">
							<input type="text" placeholder="E-mail" />
							<button class="button" type="submit">Enviar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<section class="especialidades">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-6 offset-lg-2">
				<div class="item left">
					<h2><strong class="box-shadow">Parto humanizado</strong></h2>
					<img class="especial" src="<?php echo get_template_directory_uri(); ?>/assets/img/especialidades/parto.png" />
					<img class="folha" src="<?php echo get_template_directory_uri(); ?>/assets/img/folha.png" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-lg-6 offset-lg-4">
				<div class="item">
					<h2><strong class="box-shadow">Parto Normal</strong></h2>
					<img class="especial" src="<?php echo get_template_directory_uri(); ?>/assets/img/especialidades/normal.png" />
					<img class="folha" src="<?php echo get_template_directory_uri(); ?>/assets/img/folha.png" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-lg-6 offset-lg-2">
				<div class="item left">
					<h2><strong class="box-shadow">Parto Cesáreo</strong></h2>
					<img class="especial" src="<?php echo get_template_directory_uri(); ?>/assets/img/especialidades/cesareo.png" />
					<img class="folha" src="<?php echo get_template_directory_uri(); ?>/assets/img/folha.png" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-lg-6 offset-lg-4">
				<div class="item">
					<h2><strong class="box-shadow">Parto Induzido</strong></h2>
					<img class="especial" src="<?php echo get_template_directory_uri(); ?>/assets/img/especialidades/induzido.png" />
					<img class="folha" src="<?php echo get_template_directory_uri(); ?>/assets/img/folha.png" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-lg-6 offset-lg-2">
				<div class="item left">
					<h2><strong class="box-shadow">Parto Natural</strong></h2>
					<img class="especial" src="<?php echo get_template_directory_uri(); ?>/assets/img/especialidades/natural.png" />
					<img class="folha" src="<?php echo get_template_directory_uri(); ?>/assets/img/folha.png" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-lg-6 offset-lg-4">
				<div class="item">
					<h2><strong class="box-shadow">Analgesia no Parto</strong></h2>
					<img class="especial" src="<?php echo get_template_directory_uri(); ?>/assets/img/especialidades/analgesia.png" />
					<img class="folha" src="<?php echo get_template_directory_uri(); ?>/assets/img/folha.png" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-lg-6 offset-lg-2">
				<div class="item left">
					<h2><strong class="box-shadow">Plano de parto</strong></h2>
					<img class="especial" src="<?php echo get_template_directory_uri(); ?>/assets/img/especialidades/plano.png" />
					<img class="folha" src="<?php echo get_template_directory_uri(); ?>/assets/img/folha.png" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-lg-6 offset-lg-4">
				<div class="item">
					<h2>
						<strong class="box-shadow">Exames</strong>
						<span>necessários para<br/> engravidar</span>
					</h2>
					<img class="especial" src="<?php echo get_template_directory_uri(); ?>/assets/img/especialidades/exames.png" />
					<img class="folha" src="<?php echo get_template_directory_uri(); ?>/assets/img/folha.png" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-lg-6 offset-lg-2">
				<div class="item left">
					<h2>
						<strong class="box-shadow">Ciclo menstrual</strong>
						<span>ovulação e<br/> período fértil</span>
					</h2>
					<img class="especial" src="<?php echo get_template_directory_uri(); ?>/assets/img/especialidades/ciclo.png" />
					<img class="folha" src="<?php echo get_template_directory_uri(); ?>/assets/img/folha.png" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-lg-6 offset-lg-4">
				<div class="item">
					<h2>
						<strong class="box-shadow">Suplementação</strong>
						<span>vitamínica para<br/> gestantes</span>
					</h2>
					<img class="especial" src="<?php echo get_template_directory_uri(); ?>/assets/img/especialidades/suplementacao.png" />
					<img class="folha" src="<?php echo get_template_directory_uri(); ?>/assets/img/folha.png" />
				</div>
			</div>
		</div>
	</div>
</section>

<section class="dra">
	<img class="dra" src="<?php echo get_template_directory_uri(); ?>/assets/img/dra.png" />
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-4 offset-lg-3 detalhes">
				<img class="logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/cristiane.png" />
				<span>Ginecologia &bull; Obstetrícia</span>
				<span>CRM-AM 3.506   RQE 1.488</span>
				<h2>Humanização,<br/> personalização e disponibilidade. </h2>
				<p>Oferecer um atendimento acolhedor e com empatia a quem me procura e baseado em evidências científicas.</p>
				<p>São 20 anos de assistência humanizada à mulher em todas as sua fases da vida, em especial em sua gestação e parto. E sempre buscando a excelência.</p>
				<button class="button">Saiba mais</button>
			</div>
		</div>
	</div>
</section>

<section class="agendamento2">
	<img class="folha" src="<?php echo get_template_directory_uri(); ?>/assets/img/bgPre.png" />
	<img class="cor" src="<?php echo get_template_directory_uri(); ?>/assets/img/bgPre2.png" />
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-7">
				<h2 class="h2">Pré-<br>agendamento</h2>
				<form class="box-shadow forms">
					<span class="txt txt1">Humanização, personalização e disponibilidade. Um atendimento ginecológico e obstétrico com mais empatia.</span>
					<span class="txt txt2">Preencha seus dados abaixo e nossa equipe entrará em contato<br> para realizar o seu agendamento.</span>
					<div class="row">
						<div class="col-12 col-lg-6">
							<input type="text" placeholder="Nome completo" />
							<input type="text" placeholder="Celular" />
						</div>
						<div class="col-12 col-lg-6">
							<input type="text" placeholder="E-mail" />
							<button class="button" type="submit">Enviar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<?php $query = new WP_Query(array( 
	'post_type' => 'post',
	'posts_per_page' => 6,
	'order' => 'DESC'
)); ?>
<?php if($query->have_posts()): ?>
<section class="blog">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-7">
				<h2 class="h2">Blogposts</h2>
				<h3>Confira os textos mais recentes publicados</h3>
				<div class="slider">
					<div class="sliderBlog">
					<?php while($query->have_posts()): $query->the_post(); ?>
						<div class="item">
							<a href="<?php the_permalink(); ?>">
								<div class="thumb">
									<img class="fios" src="<?php echo get_template_directory_uri(); ?>/assets/img/fios.png" />
									<div style="background-image: url(<?php the_post_thumbnail_url(); ?>);" class="img"></div>
								</div>
								<h2><?php the_title(); ?></h2>
								<?php the_excerpt(); ?>
							</a>
						</div>
					<?php endwhile; ?>
					</div>
				</div>
				<a href="<?php echo home_url('blog'); ?>">
					<button class="button">Todos os blogposts</button>
				</a>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>

<?php $query = new WP_Query(array( 
	'post_type' => 'ebook',
	'posts_per_page' => 1,
	'order' => 'DESC'
)); ?>
<?php if($query->have_posts()): ?>
<section class="ebooks">
	<img class="bg" src="<?php echo get_template_directory_uri(); ?>/assets/img/bgEbooks.png" />
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-10 offset-lg-1 align-self-center">
				<h2 class="h2">E-books</h2>
			</div>
			<div class="detalhes col-12 col-lg-4 offset-lg-1 align-self-center">
			<?php while($query->have_posts()): $query->the_post(); ?>
				<h2><?php the_title(); ?></h2>
				<?php the_post_thumbnail(); ?>
				<?php the_field('descricao_curta'); ?>
				<a href="<?php the_permalink(); ?>">
					<button class="button">Baixe gratuitamente</button>
				</a>
			<?php endwhile; ?>
			</div>
			<div class="col-12 align-self-center">
				<button class="button todos">Todos os e-books</button>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>

<section class="moca">
	<img class="moca" src="<?php echo get_template_directory_uri(); ?>/assets/img/moca.png" />
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-4 offset-lg-4 align-self-center">
				<h2 class="h2">Newsletter</h2>
				<p>Deixe seu e-mail e receba novidades e informações.</p>
				<form class="forms">
					<input type="text" placeholder="Nome completo" />
					<input type="text" placeholder="E-mail" />
					<button class="button" type="submit">Enviar</button>
				</form>
			</div>
		</div>
	</div>
</section>

<?php get_footer();
