<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Dra._Cristiane_Pacheco
 */

?>	
	</main><!-- #main -->
	<footer id="colophon" class="site-footer">
		<div class="redeIcon desktop">
			<a target="_blank" href="https://wa.me/5592991731213">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/folhalateralWP.png" />
			</a>
			<a target="_blank" href="tel:559232211560">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/folhalateralTP.png" />
			</a>
			<a target="_blank" href="https://instagram.com/cristiane_pacheco">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/folhalateralIG.png" />
			</a>
			<a target="_blank" href="https://www.facebook.com/dra.crispacheco">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/folhalateralFB.png" />
			</a>
		</div>
		<div class="redeIcon mobile">
			<a target="_blank" href="tel:559232211560">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/folhalateralTPM.png" />
			</a>
			<a target="_blank" href="https://instagram.com/cristiane_pacheco">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/folhalateralIGM.png" />
			</a>
			<a target="_blank" href="https://www.facebook.com/dra.crispacheco">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/folhalateralFBM.png" />
			</a>
		</div>

		<a target="_self" href="<?php echo home_url('agendamento'); ?>" class="agendaIcon">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/preagendamento.png" />
			<span>Faça seu <strong>pré-agendamento</strong> de consulta aqui</span>
		</a>

		<a target="_blank" href="https://wa.me/5592991731213" class="whatsIcon">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/wp.png" />
		</a>
		<div class="container">
			<div class="site-info row justify-content-center">
				<div class="col-12 endereco">
					<a target="_blank" href="https://goo.gl/maps/uSHfso426dknaTZE7">
						<span class="center"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/pinicon.png" /> Av. Jornalista Umberto Calderaro Filho, 455  •  Cristal Tower  •  Sala 1817<br/> Adrianópolis  •  Manaus-AM  •  CEP 69057-015</span>
					</a>
				</div>
				<div class="col-12 col-lg-4 detalhes">
					<a target="_blank" href="tel:559232211560">
						<span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/foneicon.png" /> (92) 3221-1560</span>
					</a>
					<span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/whatsicon.png" /> <a target="_blank" href="https://wa.me/5592991731213">(92) 99173-1213</a> • <a target="_blank" href="https://wa.me/5592991428155">(92) 991428155</a></span>

					<a class="escondemail" data-email="contatoARROBAdracristianepachecoPONTOcomPONTO2br">
						<span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/mailicon.png"> <span class="mail">Clique para exibir o e-mail</span></span>
					</a>

				</div>
				<div class="col-12 col-lg-4 detalhes">
					
					<a href="<?php echo home_url('agendamento'); ?>">
						<span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/calicon.png" /> Pré-agende sua consulta aqui</span>
					</a>

					<a target="_blank" href="https://instagram.com/cristiane_pacheco">
						<span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/instaicon.png" /> @cristiane_pacheco</span>
					</a>

					<a target="_blank" href="https://www.facebook.com/dra.crispacheco">
						<span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/faceicon.png" /> Dra. Cristiane Pacheco</span>
					</a>

				</div>
				<div class="col-12 col-lg-8 creditos">
					<a href="<?php echo home_url(); ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logofooter.png" class="logo" />
					</a>
					<p>2020 ©️ Todos os direitos reservados. O conteúdo deste site foi elaborado pela equipe da Dra. Cristiana Pacheco e as informações aqui contidas têm caráter meramente informativo e educacional. Não deve ser utilizado para realizar autodiagnóstico ou automedicação. Em caso de dúvidas, consulte seu médico. Somente ele está habilitado a praticar o ato médico, conforme recomendação do Conselho Federal de Medicina.<br/>

					Todas as imagens contidas no site são meramente ilustrativas e foram compradas em banco de imagens, portanto não utilizam imagens de pacientes.<br/>

					Diretora Clínica Responsável: Cristiana Pacheco - CRM-AM 3.506, RQE 1.488.</p>
				</div>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
