<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Dra._Cristiane_Pacheco
 */

get_header('ebooks'); ?>
<section class="obrigado">
	<div class="container-fluid">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/folhaobg.png" />
		<div class="row">
			<div class="col-12 col-md-3 offset-md-3 align-self-center">
				<h3 class="bra">E-books</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-md-3 offset-md-3 align-self-center">
				<div class="conteudo">
					<h3>Muito obrigado.</h3>
					<h4>Em breve você receberá o e-book em seu e-mail cadastrado.</h4>
					<p>Esperamos que esse conteúdo possa ajudar nos esclarecimentos do tema abordado.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row justify-content-around">

			<div class="col-12 col-lg-4">
				<button>Agende sua consulta</button>
			</div>

			<div class="col-12 col-lg-4">
				<p class="dou">Caso ainda tenha dúvidas, entre em contato conosco por meio do whatsapp, no botão disponível logo abaixo.</p>
			</div>
			
			
		</div>
	</div>
</section>
<?php get_footer();