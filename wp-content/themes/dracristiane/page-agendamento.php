<?php get_header(); ?>
	<?php while(have_posts()): the_post(); ?>
	
	<section class="page-agendamento">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-lg-8">
					<h2 class="h2">Pré- Agendamento</h2>
					<h3>Humanização, personalização e disponibilidade. Um atendimento ginecológico e obstétrico com mais empatia.</h3>
					<h4>Preencha seus dados abaixo e nossa equipe entrará em contato para realizar o seu agendamento.</h3>
					<div id="mauticform_wrapper_agendamentositedracristianepacheco" class="mauticform_wrapper">
					    <form class="forms box-shadow" autocomplete="false" role="form" method="post" action="http://mkt.dracristianepacheco.com.br/form/submit?formId=1" id="mauticform_agendamentositedracristianepacheco" data-mautic-form="agendamentositedracristianepacheco" enctype="multipart/form-data">


							<span class="txt txt1">Humanização, personalização e disponibilidade. Um atendimento ginecológico e obstétrico com mais empatia.</span>
							<span class="txt txt2">Preencha seus dados abaixo e nossa equipe entrará em contato<br> para realizar o seu agendamento.</span>

					        <div class="mauticform-error" id="mauticform_agendamentositedracristianepacheco_error"></div>
					        <div class="mauticform-message" id="mauticform_agendamentositedracristianepacheco_message"></div>
					        <div class="mauticform-innerform">

					            
					          <div class="mauticform-page-wrapper mauticform-page-1 row" data-mautic-form-page="1">

				          	<div class="col-12 col-lg-6">

					            <div id="mauticform_agendamentositedracristianepacheco_nome" data-validate="nome" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-1 mauticform-required">
					                <input id="mauticform_input_agendamentositedracristianepacheco_nome" name="mauticform[nome]" value="" placeholder="Nome completo" class="mauticform-input" type="text">
					                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
					            </div>

					            <div id="mauticform_agendamentositedracristianepacheco_email" data-validate="email" data-validation-type="email" class="mauticform-row mauticform-email mauticform-field-2 mauticform-required">
					                <input id="mauticform_input_agendamentositedracristianepacheco_email" name="mauticform[email]" value="" placeholder="E-mail" class="mauticform-input" type="email">
					                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
					            </div>


					       	</div>
					       	<div class="col-12 col-lg-6">

					            <div id="mauticform_agendamentositedracristianepacheco_celular" data-validate="celular" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-3 mauticform-required">
					                <input id="mauticform_input_agendamentositedracristianepacheco_celular" name="mauticform[celular]" value="" placeholder="Celular" class="mauticform-input" type="text">
					                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
					            </div>

					            <div id="mauticform_agendamentositedracristianepacheco_enviar" class="mauticform-row mauticform-button-wrapper mauticform-field-4">
					                <button class="button" type="submit" name="mauticform[enviar]" id="mauticform_input_agendamentositedracristianepacheco_enviar" value="" class="mauticform-button btn btn-default">Enviar</button>
					            </div>
					       </div>
					            </div>
					        </div>

					        <input type="hidden" name="mauticform[formId]" id="mauticform_agendamentositedracristianepacheco_id" value="1">
					        <input type="hidden" name="mauticform[return]" id="mauticform_agendamentositedracristianepacheco_return" value="">
					        <input type="hidden" name="mauticform[formName]" id="mauticform_agendamentositedracristianepacheco_name" value="agendamentositedracristianepacheco">

					        </form>
					</div>
					<script type="text/javascript">
					/** This section is only needed once per page if manually copying **/
					if (typeof MauticSDKLoaded == 'undefined') {
					var MauticSDKLoaded = true;
					var head            = document.getElementsByTagName('head')[0];
					var script          = document.createElement('script');
					script.type         = 'text/javascript';
					script.src          = 'http://mkt.dracristianepacheco.com.br/media/js/mautic-form.js';
					script.onload       = function() {
					MauticSDK.onLoad();
					};
					head.appendChild(script);
					var MauticDomain = 'http://mkt.dracristianepacheco.com.br';
					var MauticLang   = {
					'submittingMessage': "Please wait..."
					}
					}else if (typeof MauticSDK != 'undefined') {
					MauticSDK.onLoad();
					}
					</script>

				</div>
			</div>
		</div>
	</section>

	<?php endwhile; ?>
<?php get_footer();