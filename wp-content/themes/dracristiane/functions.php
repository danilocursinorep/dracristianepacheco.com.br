<?php
/**
 * Dra. Cristiane Pacheco functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Dra._Cristiane_Pacheco
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'dracristiane_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function dracristiane_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Dra. Cristiane Pacheco, use a find and replace
		 * to change 'dracristiane' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'dracristiane', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'dracristiane' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'dracristiane_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'dracristiane_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function dracristiane_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'dracristiane_content_width', 640 );
}
add_action( 'after_setup_theme', 'dracristiane_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function dracristiane_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'dracristiane' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'dracristiane' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'dracristiane_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function dracristiane_scripts() {
	//jQuery	
	wp_deregister_script('jquery');
	wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js', array(), null, true);

	//Bootstrap
	wp_enqueue_script('bootstrap-js', 'https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js', array(), null, true);
	wp_enqueue_style( 'bootstrap-css', 'https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css',false,'1.1','all');

	//jQuey Mask
	wp_enqueue_script('mask-js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js', array(), null, true);

	//Vendor and Custom JS
	wp_enqueue_script('vendor-min-js', get_template_directory_uri() . '/assets/js/vendor.min.js', array(), null, true);
	wp_enqueue_script('custom-min-js', get_template_directory_uri() . '/assets/js/custom.min.js', array(), null, true);

	wp_enqueue_style( 'dracristiane-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'dracristiane-style', 'rtl', 'replace' );

	wp_enqueue_script( 'dracristiane-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'dracristiane_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

//Custom Type Vídeo
function postTypeEbook() {
   register_post_type('ebook', array(
       'labels' => array(
		'name' => __('E-books'),
		'singular_name' => __('E-book')
       ),
       'public' => true,
       'menu_position' => 5,
       'show_ui' => true,
       'capability_type' => 'page',
       'hierarchical' => false,
       'has_archive' => false,
       'rewrite' => array('slug' => 'ebooks'),
       'menu_icon' => 'dashicons-book-alt',
       'supports' => array('title', 'thumbnail')
   ));
}
add_action('init', 'postTypeEbook');

function wpb_alter_comment_form_fields($fields) {;
    unset($fields['email']);
    unset($fields['url']);
    return $fields;
}
add_filter('comment_form_default_fields', 'wpb_alter_comment_form_fields');

function relacionados( $post_id, $args = array() ) {
	$terms = get_the_terms( $post_id, 'category' );

	if ( empty( $terms ) ) $terms = array();

	$term_list = wp_list_pluck( $terms, 'slug' );

	$related_args = array(
		'post_type' => 'post',
		'posts_per_page' => 2,
		'post_status' => 'publish',
		'post__not_in' => array($post_id),
		'orderby' => 'rand',
		'tax_query' => array(
			array(
				'taxonomy' => 'category',
				'field' => 'slug',
				'terms' => $term_list
			)
		)
	);

	return new WP_Query( $related_args );
}

function pagination($query) {
	$total = $query->max_num_pages;
	$big = 999999999999999999;
	if ($total > 1) {
		$current = max(1, get_query_var('paged'));
		echo paginate_links(array(
			'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
			'format' => '?paged=%#%',
			'current' => $current,
			'prev_text' => '<img src="'.get_template_directory_uri().'/assets/img/folhapro.png" /><span>Anterior</span>',
			'next_text' => '<img src="'.get_template_directory_uri().'/assets/img/folhapro.png" /><span>Próximo</span>',
			'total' => $total,
		));
	}
}