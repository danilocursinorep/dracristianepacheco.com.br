<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package elo
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if (post_password_required()) {return;}?>
<div id="comments" class="comments-area">
		<?php $args = array(
			'fields' => apply_filters(
				'comment_form_default_fields', array(
					'author' =>'<p class="comment-form-author">' . '<input id="author" placeholder="Seu nome" required name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" /></p>',
				)
			),
			'title_reply_before' => '',
			'title_reply_after' => '',
			'comment_notes' => '',
			'comment_field' => '<p class="comment-form-comment"><textarea id="comment" name="comment" placeholder="Iniciar..." cols="45" rows="8" aria-required="true" required></textarea></p>',
			'comment_notes_after' => '',
			'title_reply' => '<div class="crunchify-text"> <h5>Deixe seu comentário!</h5></div>'
		); ?>
		<?php comment_form($args); ?>

	<?php if(have_comments()): ?>
		
		<?php the_comments_navigation(); ?>

		<ol class="comment-list">
			<?php wp_list_comments(array(
				'style' => 'ol',
				'short_ping' => true,
			)); ?>
		</ol><!-- .comment-list -->

		<?php the_comments_navigation(); ?>
		<?php if (!comments_open()): ?>
			<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'testtemplate' ); ?></p>
		<?php endif; ?>

	<?php endif; ?>

</div>
