<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Dra._Cristiane_Pacheco
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<?php the_title('<h2 class="title">', '</h2>'); ?>
	<span class="author"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/folhautor.png"> Por <?php the_author_meta('display_name'); ?></span>

	<div class="conteudo">
		<?php the_content(); ?>
	</div><!-- .entry-content -->

	<div class="share">
		<h4>Gostou desse artigo? <strong>Compartilhe :-)</strong></h4>
		<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>">
			<img class="shareB" src="<?php echo get_template_directory_uri(); ?>/assets/img/shareFace.png">
		</a>
		<a target="_blank" href="https://twitter.com/home?status=<?php the_permalink(); ?>">
			<img class="shareB" src="<?php echo get_template_directory_uri(); ?>/assets/img/shareTwitter.png">
		</a>
		<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=&summary=&source=">
			<img class="shareB" src="<?php echo get_template_directory_uri(); ?>/assets/img/shareIn.png">
		</a>
		<a target="_blank" href="https://api.whatsapp.com/send?text=<?php the_permalink(); ?>">
			<img class="shareB" src="<?php echo get_template_directory_uri(); ?>/assets/img/shareWhats.png">
		</a>
	</div>

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'dracristiane' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					wp_kses_post( get_the_title() )
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
