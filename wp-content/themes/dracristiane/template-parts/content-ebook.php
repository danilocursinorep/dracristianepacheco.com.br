<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Dra._Cristiane_Pacheco
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('row'); ?>>
	
	<div class="col-12 col-md-6 align-self-center">
		<?php the_post_thumbnail(); ?>
	</div>
	<div class="col-12 col-md-6 align-self-center">
		<h3 class="title-ebook">E-book</h3>
		<?php the_title('<h2 class="title">', '</h2>'); ?>	
	</div>

	<div class="col-12 descricao">
		<?php the_field('descricao'); ?>
	</div>
	
	<div class="col-12 col-md-6 itens align-self-start">
		<?php the_field('itens'); ?>
	</div>
	
	<div class="col-12 col-md-6 formulario align-self-center">
		<div class="form">
			<h2>Por favor, preencha seus dados para o download do e-book:</h2>
			<?php the_field('formulario_completo'); ?>
			<?php the_field('formulario_script'); ?>
		</div>
	</div>

	<div class="col-12 share">
		<h4>Gostou desse blogpost? <strong>Compartilhe :-)</strong></h4>
		<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>">
			<img class="shareB" src="<?php echo get_template_directory_uri(); ?>/assets/img/shareFaceB.png">
		</a>
		<a target="_blank" href="https://twitter.com/home?status=<?php the_permalink(); ?>">
			<img class="shareB" src="<?php echo get_template_directory_uri(); ?>/assets/img/shareTwitterB.png">
		</a>
		<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=&summary=&source=">
			<img class="shareB" src="<?php echo get_template_directory_uri(); ?>/assets/img/shareInB.png">
		</a>
		<a target="_blank" href="https://api.whatsapp.com/send?text=<?php the_permalink(); ?>">
			<img class="shareB" src="<?php echo get_template_directory_uri(); ?>/assets/img/shareWhatsB.png">
		</a>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
