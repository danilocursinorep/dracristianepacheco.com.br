<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Dra._Cristiane_Pacheco
 */

get_header('page'); ?>
	<?php while(have_posts()): the_post(); ?>
	
	<section class="page paginas">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-lg-8">
					<img class="folha" src="<?php echo get_template_directory_uri(); ?>/assets/img/folha.png">
					<?php get_template_part( 'template-parts/content', 'artigo' ); ?>
					<div class="comments">
						<?php if (comments_open() || get_comments_number()): ?>
							<?php comments_template(); ?>
						<?php endif; ?>
					</div>
					<?php $rela = relacionados(get_the_ID()); ?>
					<?php if ($rela->have_posts()): ?>
					<div class="tambem">
						<h2>Você pode gostar também:</h2>
						<div class="row">
							<?php while ($rela->have_posts()):$rela->the_post(); ?>
							<div class="item col-4 col-md-2 align-self-center">
								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail(); ?>
								</a>
							</div>
							<div class="item col-8 col-md-4 align-self-center">
								<a href="<?php the_permalink(); ?>">
									<h3><?php the_title(); ?></h3>
									<button tabindex="-1">Saiba mais &gt;</button>
								</a>
							</div>
							<?php endwhile; ?>
						</div>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
	
	<?php endwhile; ?>
<?php get_footer();