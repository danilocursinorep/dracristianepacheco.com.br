<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Dra._Cristiane_Pacheco
 */

get_header('ebooks'); ?>
<section class="single-ebook">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-10">
				<?php while (have_posts()): the_post(); ?>
					<?php get_template_part( 'template-parts/content', 'ebook' ); ?>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer();