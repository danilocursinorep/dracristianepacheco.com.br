$(document).ready(function(){
     menu();

     $('.celnum').mask('(00) 00000-0000');

     $('.preagendar').on('change', function(){
          if (($(this).val() == 'Sim') || ($(this).val() == 'sim')) {
               $('.preagendar_periodo').addClass('show');
               $('.preagendar_dia').addClass('show');
          } else {
               $('.preagendar_periodo').removeClass('show');
               $('.preagendar_dia').removeClass('show');
          }
     });

     $('select').on('change', function(){
          if ($(this).val()) {
               $(this).addClass('val');
          } else {
               $(this).removeClass('val');
          }
     });
     
     $('#menu-icone').on('click', function(){
          $(this).toggleClass('open');
          if ($('#site-navigation .menu-container').hasClass('show')) {
               $('#site-navigation .menu-container').removeClass('show');
               setTimeout(function() {
                    $('#site-navigation .menu-container').removeClass('open');
               }, 400);
          } else {
               $('#site-navigation .menu-container').addClass('open');
               setTimeout(function() {
                    $('#site-navigation .menu-container').addClass('show');
               }, 1);
          }
     });

     $('li.menu-item-has-children > a').on('click', function(e){
          e.preventDefault();
          $(this).parent().parent('ul').toggleClass('hidden');
          $(this).parent().toggleClass('show');

          if($(this).hasClass('lastMenu')) {
               $(this).removeClass('lastMenu');
               $(this).parent('li').parent('.sub-menu').parent('li').children('a').addClass('lastMenu');
          } else {
               $('.lastMenu').removeClass('lastMenu');
               $(this).addClass('lastMenu');
          }

          $(this).next('.sub-menu').toggleClass('show');
     });

     $('.slider .sliderBlog').slick({
          dots: false,
          arrows: true,
          infinite: true,
          slidesToShow: 2,
          infinite: false,
          slidesToScroll: 1,
          // centerMode: true,
          // variableWidth: false,
          responsive: [
               {
                    breakpoint: 768,
                    settings: {
                         slidesToShow: 1,
                         slidesToScroll: 1
                    }
               }
          ]
     });
});

     
$(window).scroll(function(){
     menu();
});

function menu() {
     if ($(window).scrollTop() > 30)  {
          $('.floatMenu').addClass('open');
          // $('#logo')
          //      .addClass('col-md-6 order-md-1 order-lg-2');
          // $('#search')
          //      .removeClass('col-lg-6')
          //      .addClass('col-lg-5 order-lg-3');
          // $('#site-navigation')
          //      .removeClass('col-lg-12')
          //      .addClass('col-md-6 col-lg-1 order-md-2 order-lg-1');

          $('.scrollTop').addClass('show');
     }  else {
          $('.floatMenu').removeClass('open');

          // $('#logo')
          //      .removeClass('col-md-6 order-md-1 order-lg-2');
          // $('#search')
          //      .removeClass('col-lg-5 order-lg-3')
          //      .addClass('col-lg-6');
          // $('#site-navigation')
          //      .removeClass('col-md-6 col-lg-1 order-md-2 order-lg-1')
          //      .addClass('col-lg-12');

          $('.scrollTop').removeClass('show');
     }
}

$('.escondemail').on('click',function(event) {
   event.preventDefault();
   $(this).off("click");
   var email = $(this).attr("data-email").replace(/ARROBA/,'@').replace(/PONTO/,'.').replace(/PONTO2/,'.');
   $(this).removeClass("escondemail");
   $(this).children('span').children('.mail').html(email);
   $(this).attr("href","mailto:"+email);
});