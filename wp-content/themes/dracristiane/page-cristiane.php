<?php get_header('cristiane'); ?>
	<?php while(have_posts()): the_post(); ?>
	<section class="dra-page">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-lg-8">
					<h2>Humanização,<br/> personalização e disponibilidade.</h2>

					<span>Oferecer um atendimento acolhedor e com empatia a quem me procura e baseado em evidências científicas.</span>

					<span>São 20 anos de assistência humanizada à mulher em todas as sua fases da vida, em especial em sua gestação e parto. E sempre buscando a excelência.</span>

					<h3>Formação</h3>

					<p>Dra. Cristiane Azevedo Oliveira Pacheco é formada em Medicina pela Universidade Federal do Amazonas (UFAM), em 1999. Fez residência médica em Ginecologia e Obstetrícia no Hospital Servidores do Estado do Rio de Janeiro, de 2000 a 2003. Tem especialização em Colposcopia, Gestação de alto risco e Urgências obstétricas. Pela Pontifícia Universidade Católica de Goiás, atualmente faz Pós-graduação em Ultrassonografia e obstetrícia.</p>

					<h4>Vasta experiência em:</h4>

					<ul>
						<li>parto humanizado, menopausa, citologia clínica, endometriose, distúrbios menstruais e HPV.</li>
					</ul>
					<a href="<?php echo home_url('agendamento'); ?>">
						<button>Agende sua consulta</button>
					</a>
				</div>
			</div>
		</div>
	</section>

	<?php endwhile; ?>
<?php get_footer();