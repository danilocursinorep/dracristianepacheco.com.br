<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Dra._Cristiane_Pacheco
 */

get_header(); ?>

<?php $s = get_search_query(); ?>

<section class="pSearch">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-10">
				<?php $query = new WP_Query(array(
					's' => $s,
					'post_type' => array('page', 'ebook', 'post'),
					'orderby' => 'rand',
					'posts_per_page' => -1,
				)); ?>
				<?php if($query->have_posts()): ?>
				<h2>Os resultados para sua busca por "<?php echo $s; ?>":</h2>

				<ul class="row">
					<?php while ($query->have_posts()):$query->the_post(); ?>
					<li class="col-12 col-md-6<?php
								if (get_page_template_slug(get_the_ID())) {
									echo ' inst';
								} elseif (get_post_type() == 'page') {
									echo ' page';
								} elseif (get_post_type() == 'ebook') {
									echo ' book';
								} elseif (get_post_type() == 'post') {
									echo ' blog';
								}
							?>">
						<a href="<?php the_permalink(); ?>">
							<span><?php the_title(); ?></span>
						</a>
					</li>
					<?php endwhile; ?>
				</ul>
				<?php else: ?>
					<h2 class="bege">Nenhum resultado para a palavra chave "<?php echo $s; ?>".<br> Tente novamente com uam nova pesquisa.</h2>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>

<section class="blog404">
	<div class="container">
		<?php $query = new WP_Query(array(
			'post_type' => 'post',
			'posts_per_page' => 2,
			'order' => 'DESC'
		)); ?>
		<?php if($query->have_posts()): ?>
		<div class="row justify-content-center">
			<div class="col-12 col-lg-10">
				<h2 class="titulo">Algumas sugestões de conteúdo para você:</h2>
				<div class="row">
					<?php while ($query->have_posts()):$query->the_post(); ?>
					<div class="item col-5 col-md-3 align-self-center">
						<a href="<?php the_permalink(); ?>">
							<div class="thumb">
								<img class="fios" src="<?php echo get_template_directory_uri(); ?>/assets/img/fios.png" />
								<div style="background-image: url(<?php the_post_thumbnail_url(); ?>);" class="img"></div>
							</div>
						</a>
					</div>
					<div class="item col-7 col-md-3 align-self-center">
						<a href="<?php the_permalink(); ?>">
							<h2>Blog</h2>
							<h3><?php the_title(); ?></h3>
							<button>Leia o blogpost</button>
						</a>
					</div>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<?php $query = new WP_Query(array(
			'post_type' => 'ebook',
			'posts_per_page' => 1,
			'order' => 'DESC'
		)); ?>
		<?php if($query->have_posts()): ?>
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="row justify-content-center">
					<?php while ($query->have_posts()):$query->the_post(); ?>
					<div class="item text-center col-12 col-lg-4">
						<a href="<?php the_permalink(); ?>">
							<?php the_post_thumbnail(); ?>
							<h2>E-book</h2>
							<h3><?php the_title(); ?></h3>
							<button>Acesse mais</button>
						</a>
					</div>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>
</section>

<?php get_footer();