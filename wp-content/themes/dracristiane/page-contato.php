<?php get_header(); ?>
	<?php while(have_posts()): the_post(); ?>
	
	<section class="page contato">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-lg-8">
					<h2 class="h2">Contato</h2>
					<h3 class="h3">Entre em contato, por telefone ou mensagem.</h3>

					<div class="itens">
						<a target="_blank" href="tel:559232211560">
							<span><img class="h" src="<?php echo get_template_directory_uri(); ?>/assets/img/foneicon.png"> (92) 3221-1560</span>
						</a>

						<span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/whatsicon.png"> <a target="_blank" href="https://wa.me/5592991731213">(92) 99173-1213</a><br/> <a target="_blank" href="https://wa.me/5592991428155">(92) 991428155</a></span>

						<a class="escondemail" data-email="contatoARROBAdracristianepachecoPONTOcomPONTO2br">
							<span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/mailicon.png"> <span class="mail">Clique para exibir o e-mail</span></span>
						</a>
						<a target="_blank" href="https://goo.gl/maps/uSHfso426dknaTZE7">
							<span><img class="h" src="<?php echo get_template_directory_uri(); ?>/assets/img/piniconC.png">Av. Jornalista Umberto Calderaro Filho, 455 - Sala 1817<br/> Edifício: Cristal Tower<br>Bairro: Adrianópolis<br/>CEP: 69057-015<br/> Manaus-AM</span>
						</a>
					</div>



					<div id="mauticform_wrapper_contatositedracristianepacheco" class="mauticform_wrapper">
					    <form autocomplete="false" role="form" method="post" action="https://mkt.dracristianepacheco.com.br/form/submit?formId=2" id="mauticform_contatositedracristianepacheco" data-mautic-form="contatositedracristianepacheco" enctype="multipart/form-data" class="form">

						<h4>Ficou com alguma dúvida? Fale com a gente.</h4>

					        <div class="mauticform-error" id="mauticform_contatositedracristianepacheco_error"></div>
					        <div class="mauticform-message" id="mauticform_contatositedracristianepacheco_message"></div>
					        <div class="mauticform-innerform">

					            
					          <div class="mauticform-page-wrapper mauticform-page-1 row" data-mautic-form-page="1">

							<div class="col-12">
					            <div id="mauticform_contatositedracristianepacheco_nome" data-validate="nome" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-1 mauticform-required">
					                <input id="mauticform_input_contatositedracristianepacheco_nome" name="mauticform[nome]" value="" placeholder="Nome completo" class="mauticform-input" type="text">
					                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
					            </div>
					       	</div>
							<div class="col-12 col-md-6">

					            <div id="mauticform_contatositedracristianepacheco_email" data-validate="email" data-validation-type="email" class="mauticform-row mauticform-email mauticform-field-2 mauticform-required">
					                <input id="mauticform_input_contatositedracristianepacheco_email" name="mauticform[email]" value="" placeholder="E-mail" class="mauticform-input" type="email">
					                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
					            </div>
					       	</div>

							<div class="col-12 col-md-6">
					            <div id="mauticform_contatositedracristianepacheco_celular" data-validate="celular" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-3 mauticform-required">
					                <input id="mauticform_input_contatositedracristianepacheco_celular" name="mauticform[celular]" value="" placeholder="Celular" class="mauticform-input celnum" type="text">
					                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
					            </div>
					       	</div>

							<div class="col-12">
					            <div id="mauticform_contatositedracristianepacheco_mensagem" data-validate="mensagem" data-validation-type="textarea" class="mauticform-row mauticform-text mauticform-field-4 mauticform-required">
					                <textarea placeholder="Mensagem" id="mauticform_input_contatositedracristianepacheco_mensagem" name="mauticform[mensagem]" class="mauticform-textarea"></textarea>
					                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
					            </div>

					            <div id="mauticform_contatositedracristianepacheco_enviar" class="mauticform-row mauticform-button-wrapper mauticform-field-5">
					                <button class="button" type="submit" name="mauticform[enviar]" id="mauticform_input_contatositedracristianepacheco_enviar" value="" class="mauticform-button btn btn-default">Enviar</button>
					            </div>
					            </div>
					        </div>

					        <input type="hidden" name="mauticform[formId]" id="mauticform_contatositedracristianepacheco_id" value="2">
					        <input type="hidden" name="mauticform[return]" id="mauticform_contatositedracristianepacheco_return" value="">
					        <input type="hidden" name="mauticform[formName]" id="mauticform_contatositedracristianepacheco_name" value="contatositedracristianepacheco">

					        </form>
					</div>
					<div class="row redes">
						<div class="col-12">
							<h4>Visite nossas redes sociais:</h4>
						</div>
						<div class="col-12 col-md-6">
							<a target="_blank" href="https://instagram.com/cristiane_pacheco"><span><img class="h" src="<?php echo get_template_directory_uri(); ?>/assets/img/instaicon.png"> @cristiane_pacheco</span></a>
						</div>
						<div class="col-12 col-md-6">
							<a target="_blank" href="https://www.facebook.com/dra.crispacheco"><span><img class="h" src="<?php echo get_template_directory_uri(); ?>/assets/img/faceicon.png"> Dra. Cristiane Pacheco</span></a>
						</div>
					</div>

				</div>
			</div>
		</div>
	</section>
	<script type="text/javascript">
		/** This section is only needed once per page if manually copying **/
		if (typeof MauticSDKLoaded == 'undefined') {
			var MauticSDKLoaded = true;
			var head = document.getElementsByTagName('head')[0];
			var script = document.createElement('script');
			script.type = 'text/javascript';
			script.src = 'https://mkt.dracristianepacheco.com.br/media/js/mautic-form.js';
			script.onload = function() {
				MauticSDK.onLoad();
			};
			head.appendChild(script);
			var MauticDomain = 'https://mkt.dracristianepacheco.com.br';
			var MauticLang   = {
				'submittingMessage': "Por favor, aguarde..."
			}
		} else if (typeof MauticSDK != 'undefined') {
			MauticSDK.onLoad();
		}
	</script>

	<?php endwhile; ?>
<?php get_footer();